'use strict';

const sUtil = require( '../lib/util' );

const canonicalize = require( '../function-schemata/javascript/src/canonicalize.js' );
const { ReferenceResolver } = require( '../src/db.js' );
const { Evaluator } = require( '../src/Evaluator.js' );
const { Invariants } = require( '../src/Invariants.js' );
const { getLogger } = require( '../src/logger.js' );
const { orchestrate } = require( '../src/orchestrate.js' );
const { getEvaluatorConfigsForEnvironment, makeWrappedResultEnvelope, responseEnvelopeContainsError } = require( '../src/utils.js' );
const { ZWrapper } = require( '../src/ZWrapper.js' );

const wikiUri = process.env.WIKI_API_URL || null;

/**
 * The main router object
 */
const router = sUtil.router();

async function propagateResult( req, res, result, logger ) {
	if ( res.writableEnded ) {
		return;
	}

	// result should be bare JSON (never a ZWrapper), so convert to JSON and log
	// an error if it is a ZWrapper.
	if ( result instanceof ZWrapper ) {
		logger.error(
			'trace/req', {
				msg: 'propagateResult has erroneously received a ZWrapper',
				response: result,
				// We repeat the incoming request ID so we can match up load
				'x-request-id': req.context.reqId
			} );
		result = result.asJSON();
	}
	logger.log(
		'trace/req', {
			msg: 'Outgoing response',
			response: result,
			// We repeat the incoming request ID so we can match up load
			'x-request-id': req.context.reqId
		} );

	const canonicalized = canonicalize( result, /* withVoid= */ true );

	if ( responseEnvelopeContainsError( canonicalized ) ) {
		// If canonicalization fails, return normalized form instead.
		logger.info( 'Could not canonicalize; outputting in normal form.' );
	} else {
		result = canonicalized.Z22K1;
	}
	res.json( result );
}

/** ROUTE DECLARATIONS GO HERE */
router.post( '/', async function ( req, res ) {

	const logger = getLogger();
	const timer = setTimeout(
		async function () {
			await propagateResult(
				req,
				res,
				makeWrappedResultEnvelope(
					null,
					{
						Z1K1: {
							Z1K1: 'Z9',
							Z9K1: 'Z5'
						},
						// TODO (T327275): Figure out what error this should actually be.
						Z5K1: {
							Z1K1: 'Z9',
							Z9K1: 'Z558'
						},
						Z5K2: {
							Z1K1: 'Z6',
							Z6K1: 'Function call timed out'
						}
					}
				).asJSON(),
				logger
			);
		},
		// TODO (T323049): Parameterize this.
		20000
	);

	function getRemainingTime() {
		return timer._idleStart + timer._idleTimeout - Date.now();
	}

	try {
		const ZObject = req.body.zobject;
		const useReentrance = req.body.useReentrance || false;
		const evaluatorConfigs = getEvaluatorConfigsForEnvironment( process.env, useReentrance );

		// Capture all stray config.
		const orchestratorConfig = {
			doValidate: req.body.doValidate || false
		};

		// Initialize invariants.
		const resolver = new ReferenceResolver( wikiUri );
		const evaluators = evaluatorConfigs.map(
			( evaluatorConfig ) => new Evaluator( evaluatorConfig ) );
		const invariants = new Invariants(
			resolver, evaluators, orchestratorConfig, getRemainingTime,
			req.context.reqId );
		// Orchestrate!
		const response = await orchestrate( ZObject, invariants );
		clearTimeout( timer );
		await propagateResult( req, res, response, logger );
	} catch ( error ) {
		logger.trace( error );
		await propagateResult(
			req,
			res,
			makeWrappedResultEnvelope(
				null,
				{
					Z1K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z5'
					},
					// TODO (T327275): Figure out what error this should actually be.
					Z5K1: {
						Z1K1: 'Z9',
						Z9K1: 'Z558'
					},
					Z5K2: {
						Z1K1: 'Z6',
						Z6K1: 'Orchestration generally failed.'
					}
				}
			).asJSON(),
			logger
		);
	}
} );

router.get( '/', function ( req, res ) {
	res.sendStatus( 200 );
} );

module.exports = function ( appObj ) {

	// the returned object mounts the routes on
	// /{domain}/vX/mount/path
	return {
		path: '/evaluate',
		api_version: 1, // must be a number!
		router: router
	};

};
