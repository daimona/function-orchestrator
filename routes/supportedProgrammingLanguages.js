'use strict';

const sUtil = require( '../lib/util' );
const { getEvaluatorConfigsForEnvironment } = require( '../src/utils.js' );

/**
 * The main router object
 */
const router = sUtil.router();

router.get( '/', function ( req, res ) {
	const evaluatorConfigs = getEvaluatorConfigsForEnvironment(
		process.env, /* useReentrance= */ false );
	const programmingLanguages = [];
	for ( const evaluatorConfig of evaluatorConfigs ) {
		for ( const language of evaluatorConfig.programmingLanguages ) {
			programmingLanguages.push( language );
		}
	}
	res.json( programmingLanguages );
} );

module.exports = function ( appObj ) {

	// the returned object mounts the routes on
	// /{domain}/vX/mount/path
	return {
		path: '/supported-programming-languages',
		api_version: 1, // must be a number!
		router: router
	};

};
