/**
 * Util classes and functions involving mocking. Used mainly for
 * testing and performance benchmark purposes.
 */

'use strict';

const fs = require( 'fs' );
const { readJSON } = require( '../src/fileUtils.js' );
const { Evaluator } = require( '../src/Evaluator.js' );
const { EvaluatorError } = require( '../src/implementation.js' );
const { Invariants } = require( '../src/Invariants.js' );
const { ReferenceResolver } = require( '../src/db.js' );
const path = require( 'path' );
const { rest } = require( 'msw' );
const normalize = require( '../function-schemata/javascript/src/normalize.js' );
const { getWrappedZObjectFromVersionedBinary } = require( '../function-schemata/javascript/src/serialize.js' );

/**
 * A simple stub for mediaWiki API that holds fake ZObjects. When fake
 * ZObjects are not set, real ones from the file system are used.
 *
 * How to use: set up with your expected values in the tests/benchmarks, and have
 * mock REST handlers "query" from it.
 */
class MediaWikiStub {

	constructor() {
		this.reset();
		this.zObjectDir_ = 'function-schemata/data/definitions/';
	}

	reset() {
		this.wiki_ = {};
	}

	/**
	 * Set up a fake definition for a ZID.
	 *
	 * @param {string} zid The ZID you want to set up a fake for.
	 * @param {*} value The JSON definition for this zid.
	 * @param {*} error
	 */
	setZId( zid, value = null, error = false ) {
		this.wiki_[ zid ] = {
			value: value,
			error: error
		};
	}

	/**
	 * Gets the fake JSON object for a certain ZID. If the value was not
	 * previously set via setZid(), then the value will be set from the
	 * real directory and returned.
	 *
	 * @param {string} zid
	 * @return {*} A fake JSON representation of the ZID.
	 */
	getZId( zid ) {
		if ( !( zid in this.wiki_ ) ) {
			const filePath = path.join( this.zObjectDir_, zid + '.json' );
			if ( !fs.existsSync( filePath ) ) {
				throw new Error( `No persistent zobject available for ${zid}` );
			}
			const jsonObj = readJSON( filePath );
			this.setZId( zid, jsonObj );
		}
		return this.wiki_[ zid ];
	}

}

/**
 * A simple stub for the function evaluator API that holds
 * preset interactions (status code and callback) for ZIDs.
 *
 * How to use: set up with your expected values in the tests/benchmarks, and have
 * mock REST handlers "query" from it.
 */
class EvaluatorStub {
	constructor() {
		this.reset();
	}

	reset() {
		this.evaluator_ = {};
	}

	/**
	 * Set up a fake evaluator result (callback + statusCode) for a ZID.
	 *
	 * @param {string} zid
	 * @param {Function} callback A function that performs the desired outcome.
	 * @param {number} statusCode The status code to return when this ZID is queried.
	 *     Default is 200.
	 * @param {boolean} doNormalize Whether to normalize after calling callback.
	 */
	setZId( zid, callback, statusCode = 200, doNormalize = true ) {
		let wrappedCallback;
		if ( doNormalize ) {
			wrappedCallback = function ( input ) {
				return normalize( callback( input ) ).Z22K1;
			};
		} else {
			wrappedCallback = callback;
		}
		this.evaluator_[ zid ] = {
			statusCode: statusCode,
			callback: wrappedCallback
		};
	}

	/**
	 * Gets the fake evaluator status code and callback for a certain ZID.
	 * If the value was not previously set via setEvaluator, an error will
	 * be thrown.
	 *
	 * @param {string} zid
	 * @return {*} A fake evaluator response template consisted of two parts:
	 *     {statusCode: number, callback: Function}.
	 */
	getZId( zid ) {
		if ( !( zid in this.evaluator_ ) ) {
			throw new EvaluatorError( `The evaluator for ZID ${zid} was never set!` );
		}
		return this.evaluator_[ zid ];
	}
}

/**
 * Sets up the mocking for GET requests to a mediaWiki URI so that it will
 * return preset ZObjects saved in a stub.
 *
 * @param {string} mediaWikiUri The mediaWiki URI used in a orchestrator.
 * @param {MediaWikiStub} mediaWikiStub A stub that contains JSON definitions of ZIDs.
 * @return {*} A REST mock handler. Needed for server setup.
 */
function mockMediaWiki( mediaWikiUri, mediaWikiStub ) {
	return rest.get( mediaWikiUri, ( req, res, ctx ) => {
		// Gets all the ZIDs from the request.
		const zids = req.url.searchParams.get( 'zids' );
		// Compose a map of ZID to JSON definition.
		let result = {};

		for ( const ZID of zids.split( '|' ) ) {
			const retrievedObject = mediaWikiStub.getZId( ZID );
			if ( retrievedObject.error ) {
				// TODO (T333022): This behavior reflects the current wikilambda_fetch
				// API; change it if that changes.
				result = { problem: 'bad' };
				break;
			}
			result[ ZID ] = {
				wikilambda_fetch: JSON.stringify( retrievedObject.value )
			};
		}
		return res( ctx.status( 200 ), ctx.json( result ) );
	} );
}

/**
 * Sets up the mocking for POST requests to a evaluator URI so that it will
 * perform preset callbacks and return preset status codes.
 *
 * @param {string} evaluatorUri The evaluator URI used in a orchestrator.
 * @param {MediaWikiStub} evaluatorStub A stub that contains preset callbacks
 *     and status codes for ZIDs.
 * @return {*} A REST mock handler. Needed for server setup.
 */
function mockEvaluator( evaluatorUri, evaluatorStub ) {
	return rest.post( evaluatorUri, ( req, res, ctx ) => {
		const buffer = Buffer.from( req.body );
		const functionCall = getWrappedZObjectFromVersionedBinary( buffer );
		const ZID = functionCall.functionName;
		const { statusCode, callback } = evaluatorStub.getZId( ZID );
		const value = callback( functionCall );
		return res( ctx.status( statusCode ), ctx.json( value ) );
	} );
}

/**
 * Silently mock GET requests to the API running at :6254 and do nothing.
 *
 * @return {*} A REST mock handler. Needed for server setup.
 */
function mockLocalhost() {
	// eslint-disable-next-line no-unused-vars
	return rest.get( 'http://localhost:6254/*', ( req, res, ctx ) => {} );
}

function getTestInvariants( doValidate, remainingTime, wikiUri, evalUri ) {
	const resolver = new ReferenceResolver( wikiUri );
	const evaluators = [
		new Evaluator( {
			programmingLanguages: [
				'javascript-es2020', 'javascript-es2019', 'javascript-es2018',
				'javascript-es2017', 'javascript-es2016', 'javascript-es2015',
				'javascript', 'python-3-9', 'python-3-8', 'python-3-7', 'python-3',
				'python'
			],
			evaluatorUri: evalUri,
			evaluatorWs: null,
			useReentrance: false } )
	];
	const orchestratorConfig = { doValidate: doValidate };
	function getRemainingTime() {
		return remainingTime;
	}
	return new Invariants( resolver, evaluators, orchestratorConfig, getRemainingTime, 'fake-request-id' );
}

module.exports = {
	MediaWikiStub,
	EvaluatorStub,
	getTestInvariants,
	mockMediaWiki,
	mockEvaluator,
	mockLocalhost
};
