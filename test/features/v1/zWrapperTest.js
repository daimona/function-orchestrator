'use strict';

const assert = require( '../../utils/assert.js' );
const { ZWrapper } = require( '../../../src/ZWrapper' );
const { BaseFrame, EmptyFrame } = require( '../../../src/frame.js' );

describe( 'ZWrapper test', function () { // eslint-disable-line no-undef

	it( 'ZWrapper class string construction', () => { // eslint-disable-line no-undef
		const stringIsNotAZWrapper = ZWrapper.create( 'Hello I am a test string', new EmptyFrame() );

		assert.deepEqual( stringIsNotAZWrapper, 'Hello I am a test string' );
	} );

	it( 'ZWrapper class construction', () => { // eslint-disable-line no-undef
		const aReference = { Z1K1: 'Z9', Z9K1: 'Z9' };
		const aReferenceZWrapper = ZWrapper.create( aReference, new EmptyFrame() );
		assert.deepEqual( aReference, aReferenceZWrapper.asJSON() );
		assert.deepEqual( new Set( [ 'Z1K1', 'Z9K1' ] ), new Set( aReferenceZWrapper.keys() ) );
		assert.deepEqual( 'Z9', aReferenceZWrapper.Z1K1 );
	} );

	it( 'ZWrapper class construction with degenerate input', () => { // eslint-disable-line no-undef
		const degenerateObject = { Z1K1: 'Z6', Z6K1: 13 };
		assert.throws( () => {
			ZWrapper.create( degenerateObject, new EmptyFrame() );
		} );
	} );

	it( 'ZWrapper resolution', async () => { // eslint-disable-line no-undef
		const theTrueTrue = {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z40'
			},
			Z40K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z41'
			}
		};
		const georgieWrapper = ZWrapper.create( theTrueTrue, new EmptyFrame() );
		assert.deepEqual( theTrueTrue.Z1K1, georgieWrapper.original_.get( 'Z1K1' ).asJSON() );
		assert.deepEqual( false, georgieWrapper.resolved_.has( 'Z1K1' ) );

		await georgieWrapper.resolveKey( [ 'Z1K1' ], /* invariants= */ null );
		assert.deepEqual( theTrueTrue.Z1K1, georgieWrapper.resolved_.get( 'Z1K1' ).asJSON() );
	} );

	it( 'ZWrapper heuristic resolution', async () => { // eslint-disable-line no-undef
		const functionallyTheTruth = {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z40'
			},
			Z40K1: {
				Z1K1: 'Z7',
				Z7K1: {
					Z1K1: 'Z6',
					Z6K1: 'not a function'
				}
			}
		};
		const napoleonHill = ZWrapper.create( functionallyTheTruth, new EmptyFrame() );
		let theGrift = null;
		let theError = null;
		// This call throws an error because the new heuristics identify the
		// Z40K1 here as a function call even though it is not remotely a valid
		// function call. The specific error is a an implementation detail. That
		// means that this test is a change detector. Once type-checking gets
		// better again, this call to resolveKey() will fail differently, most
		// likely with a proper error in Z22K2.
		//
		// Also, we can't use assert.throws() with async code.
		try {
			theGrift = await napoleonHill.resolveKey( [ 'Z40K1' ], /* invariants= */ null );
		} catch ( error ) {
			theError = error;
		}

		assert.isNull( theGrift, 'This is insurance that an error was thrown.' );
		assert.isTrue( theError.toString().includes( 'Invalid identity' ) );
	} );

	it( 'ZWrapper debugObject', () => { // eslint-disable-line no-undef
		const nullZReferenceString = 'Z24';
		const nullZReference = { Z1K1: 'Z9', Z9K1: nullZReferenceString };
		const nullZWrapper = ZWrapper.create( nullZReference, new EmptyFrame() );
		assert.deepEqual(
			{ object_: nullZReferenceString, scope_: {} },
			nullZWrapper.debugObject()
		);

		const recursiveNullZWrapper = ZWrapper.create(
			nullZReference,
			new BaseFrame( nullZWrapper )
		);
		assert.deepEqual(
			{
				object_: nullZReferenceString,
				scope_: { object_: nullZReferenceString, scope_: {} }
			},
			recursiveNullZWrapper.debugObject()
		);

		const emptyEnvelopeObject = { Z1K1: 'Z22', Z22K1: nullZReference, Z22K2: nullZReference };

		const baseZWrapper = ZWrapper.create( emptyEnvelopeObject, new BaseFrame( nullZWrapper ) );
		assert.deepEqual(
			{
				// The nullZReferences (normal form) become nullZReferenceStrings (canonical)
				object_: { Z1K1: 'Z22', Z22K1: nullZReferenceString, Z22K2: nullZReferenceString },
				scope_: { object_: nullZReferenceString, scope_: {} }
			},
			baseZWrapper.debugObject()
		);
	} );

	it( 'ZWrapper asJSON, asJSONEphemeral respect correct keys', () => { // eslint-disable-line no-undef
		const theObject = {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z10000'
			},
			K1: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z18'
				},
				Z18K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z10005'
				}
			},
			K2: {
				Z1K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z18'
				},
				Z18K1: {
					Z1K1: 'Z9',
					Z9K1: 'Z10006'
				}
			}
		};
		const replacementStringOne = {
			Z1K1: 'Z6',
			Z6K1: 'diamond daimon monad i'
		};
		const replacementStringTwo = {
			Z1K1: 'Z6',
			Z6K1: 'adam kadmon in the sky'
		};
		const wrappedObject = ZWrapper.create( theObject, new EmptyFrame() );
		wrappedObject.setName( 'K1', replacementStringOne );
		wrappedObject.setNameEphemeral( 'K2', replacementStringTwo );

		// For asJSON, the non-ephemerally set key should be visible but not the ephemeral one.
		const actualJSON = wrappedObject.asJSON();
		const expectedJSON = { ...theObject };
		expectedJSON.K1 = replacementStringOne;
		assert.deepEqual( actualJSON, expectedJSON );

		// For asJSONEphemeral, both set keys should be visible.
		const actualJSONEphemeral = wrappedObject.asJSONEphemeral();
		const expectedJSONEphemeral = { ...theObject };
		expectedJSONEphemeral.K1 = replacementStringOne;
		expectedJSONEphemeral.K2 = replacementStringTwo;
		assert.deepEqual( actualJSONEphemeral, expectedJSONEphemeral );
	} );

} );
