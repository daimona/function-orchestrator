'use strict';

const assert = require( '../../utils/assert.js' );
const {
	createSchema,
	createZObjectKey,
	generateError,
	getEvaluatorConfigsForEnvironment,
	isError,
	isGenericType,
	isRefOrString,
	makeBoolean,
	makeWrappedResultEnvelope,
	quoteZObject,
	responseEnvelopeContainsError,
	responseEnvelopeContainsValue,
	returnOnFirstError,
	setMetadataValues,
	traverseZList
} = require( '../../../src/utils.js' );
const { ZWrapper } = require( '../../../src/ZWrapper.js' );
const { EmptyFrame } = require( '../../../src/frame.js' );
const {
	convertZListToItemArray,
	makeMappedResultEnvelope,
	makeEmptyZResponseEnvelopeMap,
	setZMapValue
} = require( '../../../function-schemata/javascript/src/utils.js' );

// Collection of objects for testing.
const createdString = { Z1K1: 'Z6', Z6K1: 'created' };

const wrappedCreatedString = ZWrapper.create( createdString, new EmptyFrame() );

const tenThou = { Z1K1: 'Z9', Z9K1: 'Z10000' };

const wrappedTenThou = ZWrapper.create( tenThou, new EmptyFrame() );

const goodZ22 = makeMappedResultEnvelope(
	{ Z1K1: 'Z6', Z6K1: 'dull but reliable sigma string' }, null
);

const badZ22 = makeMappedResultEnvelope(
	null, generateError( 'extremely exciting but morally flawed error string' )
);

const embeddedType = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z4' },
	Z4K1: {
		Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
		Z7K1: { Z1K1: 'Z9', Z9K1: 'Z831' },
		Z831K1: { Z1K1: 'Z9', Z9K1: 'Z6' }
	},
	Z4K2: {
		Z1K1: {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
			Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
			Z881K1: { Z1K1: 'Z9', Z9K1: 'Z3' }
		},
		K1: {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z3' },
			Z3K1: { Z1K1: 'Z9', Z9K1: 'Z6' },
			Z3K2: { Z1K1: 'Z6', Z6K1: 'K1' },
			Z3K3: { Z1K1: 'Z9', Z9K1: 'Z1212' }
		},
		K2: {
			Z1K1: {
				Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
				Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
				Z881K1: { Z1K1: 'Z9', Z9K1: 'Z3' }
			}
		}
	},
	Z4K3: { Z1K1: 'Z9', Z9K1: 'Z1000' }
};

const someType = {
	Z1K1: { Z1K1: 'Z9', Z9K1: 'Z4' },
	Z4K1: { Z1K1: 'Z9', Z9K1: 'Z10000' },
	Z4K2: {
		Z1K1: {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
			Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
			Z881K1: { Z1K1: 'Z9', Z9K1: 'Z3' }
		},
		K1: {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z3' },
			Z3K1: { Z1K1: 'Z9', Z9K1: 'Z6' },
			Z3K2: { Z1K1: 'Z6', Z6K1: 'Z10000K1' },
			Z3K3: { Z1K1: 'Z9', Z9K1: 'Z1212' }
		},
		K2: {
			Z1K1: {
				Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
				Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
				Z881K1: { Z1K1: 'Z9', Z9K1: 'Z3' }
			},
			K1: {
				Z1K1: { Z1K1: 'Z9', Z9K1: 'Z3' },
				Z3K1: embeddedType,
				Z3K2: {
					Z1K1: 'Z6',
					Z6K1: 'Z10000K2'
				},
				Z3K3: {
					Z1K1: 'Z9',
					Z9K1: 'Z1212'
				}
			},
			K2: {
				Z1K1: {
					Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
					Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
					Z881K1: { Z1K1: 'Z9', Z9K1: 'Z3' }
				}
			}
		}
	},
	Z4K3: { Z1K1: 'Z9', Z9K1: 'Z1000' }
};

const objectOfSomeType = {
	Z1K1: someType,
	Z10000K1: createdString,
	Z10000K2: {
		Z1K1: embeddedType,
		K1: createdString
	}
};

describe( 'utils test', function () { // eslint-disable-line no-undef

	// isRefOrString

	it( 'isRefOrString is true for ZWrapped Z6s', () => { // eslint-disable-line no-undef
		assert.deepEqual( isRefOrString( wrappedCreatedString ), true );
	} );

	it( 'isRefOrString is true for unwrapped Z6s', () => { // eslint-disable-line no-undef
		assert.deepEqual( isRefOrString( createdString ), true );
	} );

	it( 'isRefOrString is true for ZWrapped Z9s', () => { // eslint-disable-line no-undef
		assert.deepEqual( isRefOrString( wrappedTenThou ), true );
	} );

	it( 'isRefOrString is true for unwrapped Z9s', () => { // eslint-disable-line no-undef
		assert.deepEqual( isRefOrString( tenThou ), true );
	} );

	it( 'isRefOrString is false for ZWrapped whatever', () => { // eslint-disable-line no-undef
		const theObject = ZWrapper.create( {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z10000' },
			Z10000K1: { Z1K1: 'Z9', Z9K1: 'Z6' }
		}, new EmptyFrame() );
		assert.deepEqual( isRefOrString( theObject ), false );
	} );

	it( 'isRefOrString is false for unwrapped whatever', () => { // eslint-disable-line no-undef
		const theObject = {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z10000' },
			Z10000K1: { Z1K1: 'Z9', Z9K1: 'Z6' }
		};
		assert.deepEqual( isRefOrString( theObject ), false );
	} );

	// createZObjectKey

	it( 'createZObjectKey from ZWrapper', () => { // eslint-disable-line no-undef
		const theObject = ZWrapper.create( {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z10000' },
			Z10000K1: { Z1K1: 'Z9', Z9K1: 'Z6' }
		}, new EmptyFrame() );
		assert.deepEqual( createZObjectKey( theObject ).asString(), 'Z10000{"Z10000K1":"Z6"}' );
	} );

	it( 'createZObjectKey from unwrapped object', () => { // eslint-disable-line no-undef
		const theObject = {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z10000' },
			Z10000K1: { Z1K1: 'Z9', Z9K1: 'Z6' }
		};
		assert.deepEqual( createZObjectKey( theObject ).asString(), 'Z10000{"Z10000K1":"Z6"}' );
	} );

	// createSchema

	it( 'createSchema for ZWrapped Z9/Z6', () => { // eslint-disable-line no-undef
		const schema = createSchema( wrappedTenThou );
		const result = schema.validate( tenThou );
		assert.deepEqual( result, true );
	} );

	it( 'createSchema for unwrapped Z9/Z6', () => { // eslint-disable-line no-undef
		const schema = createSchema( tenThou );
		const result = schema.validate( tenThou );
		assert.deepEqual( result, true );
	} );

	it( 'createSchema for some type', () => { // eslint-disable-line no-undef
		const schema = createSchema( objectOfSomeType );
		const result = schema.validate( objectOfSomeType );
		assert.deepEqual( result, true );
	} );

	// isError

	it( 'isError with canonical probable error', () => { // eslint-disable-line no-undef
		const canonicalError = {
			Z1K1: 'Z5',
			Z5K1: 'nothing'
		};
		assert.deepEqual( isError( canonicalError ), true );
	} );

	it( 'isError with canonical normal error', () => { // eslint-disable-line no-undef
		const canonicalError = {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z5' },
			Z5K1: { Z1K1: 'Z6', Z6K1: 'nothing' }
		};
		assert.deepEqual( isError( canonicalError ), true );
	} );

	it( 'isError with non-error', () => { // eslint-disable-line no-undef
		assert.deepEqual( isError( tenThou ), false );
	} );

	// isGenericType

	it( 'isGenericType with ZWrapped generic type', () => { // eslint-disable-line no-undef
		const someList = ZWrapper.create( embeddedType.Z4K2, new EmptyFrame() );
		assert.deepEqual( isGenericType( someList ), true );
	} );

	it( 'isGenericType with generic type', () => { // eslint-disable-line no-undef
		const someList = embeddedType.Z4K2;
		assert.deepEqual( isGenericType( someList ), true );
	} );

	it( 'isGenericType with not that', () => { // eslint-disable-line no-undef
		assert.deepEqual( isGenericType( objectOfSomeType ), false );
	} );

	// setMetadataValues

	it( 'setMetadataValues', () => { // eslint-disable-line no-undef
		const envelope = ZWrapper.create( badZ22, new EmptyFrame() );
		const newValues = new Map(
			[
				[ createdString, objectOfSomeType ]
			]
		);
		const expectedMapItems = [ badZ22.Z22K2.K1.K1 ];
		assert.deepEqual( convertZListToItemArray( envelope.Z22K2.K1.asJSON() ), expectedMapItems );
		setMetadataValues( envelope, newValues );
		expectedMapItems.push( {
			Z1K1: {
				Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
				Z7K1: { Z1K1: 'Z9', Z9K1: 'Z882' },
				Z882K1: { Z1K1: 'Z9', Z9K1: 'Z6' },
				Z882K2: { Z1K1: 'Z9', Z9K1: 'Z1' }
			},
			K1: createdString,
			K2: objectOfSomeType
		} );
		// FIXME: This is not technically correct as a ZMap's items should be
		// order-agnostic. Find a way to assert unordered list equality.
		assert.deepEqual( convertZListToItemArray( envelope.Z22K2.K1.asJSON() ), expectedMapItems );
	} );

	// responseEnvelopeContainsError

	it( 'responseEnvelopeContainsError finds nothing on undefined', async () => { // eslint-disable-line no-undef
		assert.equal( responseEnvelopeContainsError(), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on an empty object', async () => { // eslint-disable-line no-undef
		assert.equal( responseEnvelopeContainsError( {} ), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on a non-ZResponseEnvelope ZObject', async () => { // eslint-disable-line no-undef
		assert.equal( responseEnvelopeContainsError( { Z1K1: 'Z6', Z6K1: 'Hello' } ), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on an undefined ZResponseEnvelope Map', async () => { // eslint-disable-line no-undef
		assert.equal( responseEnvelopeContainsError( { Z1K1: 'Z22', Z22K1: 'Z1' } ), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on a null ZResponseEnvelope Map', async () => { // eslint-disable-line no-undef
		assert.equal( responseEnvelopeContainsError( goodZ22 ), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on void ZResponseEnvelope Map', async () => { // eslint-disable-line no-undef
		assert.equal( responseEnvelopeContainsError( makeMappedResultEnvelope( 'Z1', undefined ) ), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on empty ZResponseEnvelope Map', async () => { // eslint-disable-line no-undef
		assert.equal( responseEnvelopeContainsError( makeMappedResultEnvelope( 'Z1', makeEmptyZResponseEnvelopeMap() ) ), false );
	} );

	it( 'responseEnvelopeContainsError finds nothing on error-less ZResponseEnvelope Map', async () => { // eslint-disable-line no-undef
		const map = makeEmptyZResponseEnvelopeMap();
		setZMapValue( map, { Z1K1: 'Z6', Z6K1: 'hello' }, 'Z24' );
		assert.equal( responseEnvelopeContainsError( makeMappedResultEnvelope( 'Z1', map ) ), false );
	} );

	it( 'responseEnvelopeContainsError finds a direct error', async () => { // eslint-disable-line no-undef
		assert.equal( responseEnvelopeContainsError( badZ22 ), true );
	} );

	it( 'responseEnvelopeContainsError finds nothing on error set to Z24', async () => { // eslint-disable-line no-undef
		const map = makeEmptyZResponseEnvelopeMap();
		setZMapValue( map, { Z1K1: 'Z6', Z6K1: 'errors' }, 'Z24' );
		assert.equal( responseEnvelopeContainsError( makeMappedResultEnvelope( 'Z1', map ) ), false );
	} );

	it( 'responseEnvelopeContainsError works with ZWrappers', async () => { // eslint-disable-line no-undef
		const goodWrapper = ZWrapper.create( goodZ22, new EmptyFrame() );
		const badWrapper = ZWrapper.create( badZ22, new EmptyFrame() );
		assert.equal( responseEnvelopeContainsError( goodWrapper ), false );
		assert.equal( responseEnvelopeContainsError( badWrapper ), true );
	} );

	// responseEnvelopeContainsValue

	it( 'responseEnvelopeContainsValue works with ZWrappers', async () => { // eslint-disable-line no-undef
		const goodWrapper = ZWrapper.create( goodZ22, new EmptyFrame() );
		const badWrapper = ZWrapper.create( badZ22, new EmptyFrame() );
		assert.equal( responseEnvelopeContainsValue( goodWrapper ), true );
		assert.equal( responseEnvelopeContainsValue( badWrapper ), false );
	} );

	// makeBoolean

	it( 'makeBoolean true, canonical', () => { // eslint-disable-line no-undef
		assert.deepEqual( makeBoolean( true, true ), { Z1K1: 'Z40', Z40K1: 'Z41' } );
	} );

	it( 'makeBoolean false, canonical', () => { // eslint-disable-line no-undef
		assert.deepEqual( makeBoolean( false, true ), { Z1K1: 'Z40', Z40K1: 'Z42' } );
	} );

	it( 'makeBoolean true, normal', () => { // eslint-disable-line no-undef
		assert.deepEqual(
			makeBoolean( true ),
			{
				Z1K1: { Z1K1: 'Z9', Z9K1: 'Z40' },
				Z40K1: { Z1K1: 'Z9', Z9K1: 'Z41' }
			} );
	} );

	it( 'makeBoolean false, normal', () => { // eslint-disable-line no-undef
		assert.deepEqual(
			makeBoolean( false ),
			{
				Z1K1: { Z1K1: 'Z9', Z9K1: 'Z40' },
				Z40K1: { Z1K1: 'Z9', Z9K1: 'Z42' }
			} );
	} );

	// traverseZList

	it( 'traverseZList', () => { // eslint-disable-line no-undef
		const resultList = [];
		const ZList = someType.Z4K2;
		const callback = ( tail ) => {
			resultList.push( tail.K1.Z3K2.Z6K1 );
		};
		traverseZList( ZList, callback );
		assert.deepEqual( resultList, [ 'Z10000K1', 'Z10000K2' ] );
	} );

	// returnOnFirstError

	it( 'returnOnFirstError encounters error in first function', async () => { // eslint-disable-line no-undef
		const badFunction = () => {
			return badZ22;
		};
		const goodFunction = () => {
			return goodZ22;
		};
		const result = await returnOnFirstError(
			goodZ22, [
				[ badFunction, [], 'badFunction' ],
				[ goodFunction, [], 'goodFunction' ] ] );
		assert.deepEqual( result, badZ22 );
	} );

	it( 'returnOnFirstError encounters no errors', async () => { // eslint-disable-line no-undef
		const theFunction = ( Z22K1 ) => {
			const result = { ...Z22K1 };
			result.Z6K1 = 'very ' + result.Z6K1;
			return makeMappedResultEnvelope( result, null );
		};
		const result = await returnOnFirstError(
			goodZ22, [
				[ theFunction, [], 'theFunction' ],
				[ theFunction, [], 'theFunction' ],
				[ theFunction, [], 'theFunction' ] ] );
		assert.deepEqual( result.Z22K1.Z6K1, 'very very very dull but reliable sigma string' );
	} );

	it( 'returnOnFirstError calls callback', async () => { // eslint-disable-line no-undef
		let stoolPigeon = false;
		const goodFunction = () => {
			return goodZ22;
		};
		const indicatorFunction = () => {
			stoolPigeon = true;
		};
		await returnOnFirstError(
			goodZ22, [ [ goodFunction, [], 'goodFunction' ] ], indicatorFunction );
		assert.deepEqual( stoolPigeon, true );
	} );

	it( 'returnOnFirstError omits Z22 if requested', async () => { // eslint-disable-line no-undef
		let stoolPigeon = false;
		const indicatorFunction = ( firstArgument = null ) => {
			if ( firstArgument !== null ) {
				stoolPigeon = true;
			}
			return goodZ22;
		};
		await returnOnFirstError(
			goodZ22, [
				[ indicatorFunction, [], 'indicatorFunction' ]
			], /* callback= */null, /* addZ22= */false );
		assert.deepEqual( stoolPigeon, false );
	} );

	// quoteZObject

	it( 'quoteZObject ZWrapped', async () => { // eslint-disable-line no-undef
		const quotedTenThou = quoteZObject( wrappedTenThou );
		const expected = {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z99'
			},
			Z99K1: tenThou
		};
		assert.deepEqual( quotedTenThou.asJSON(), expected );
	} );

	it( 'quoteZObject unwrapped', async () => { // eslint-disable-line no-undef
		const quotedTenThou = quoteZObject( tenThou );
		const expected = {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z99'
			},
			Z99K1: tenThou
		};
		assert.deepEqual( quotedTenThou.asJSON(), expected );
	} );

	// makeWrappedResultEnvelope

	it( 'makeWrappedResultEnvelope, good version', async () => { // eslint-disable-line no-undef
		const goodUnwrapped = makeMappedResultEnvelope(
			{ Z1K1: 'Z6', Z6K1: 'dull but reliable sigma string' }, null
		);
		const goodWrapped = makeWrappedResultEnvelope(
			{ Z1K1: 'Z6', Z6K1: 'dull but reliable sigma string' }, null
		);
		assert.deepEqual( goodWrapped.asJSON(), goodUnwrapped );
	} );

	it( 'makeWrappedResultEnvelope, bad version', async () => { // eslint-disable-line no-undef
		const badUnwrapped = makeMappedResultEnvelope(
			null, generateError( 'extremely exciting but morally flawed error string' )
		);
		const badWrapped = makeWrappedResultEnvelope(
			null, generateError( 'extremely exciting but morally flawed error string' )
		);
		assert.deepEqual( badWrapped.asJSON(), badUnwrapped );
	} );

	// getEvaluatorConfigsForEnvironment

	it( 'getEvaluatorConfigsForEnvironment with ORCHESTRATOR_CONFIG (overrides useReentrance)', async () => { // eslint-disable-line no-undef
		const evaluatorConfigs = [
			{
				programmingLanguages: [ 'chef' ],
				evaluatorUri: 'nowhere',
				evaluatorWs: 'somewhere',
				useReentrance: false
			}
		];
		const environment = {
			ORCHESTRATOR_CONFIG: JSON.stringify( { evaluatorConfigs: evaluatorConfigs } )
		};
		const actualConfigs = getEvaluatorConfigsForEnvironment( environment, true );
		assert.deepEqual( actualConfigs, evaluatorConfigs );
	} );

	it( 'getEvaluatorConfigsForEnvironment default', async () => { // eslint-disable-line no-undef
		const expectedConfigs = [
			{
				programmingLanguages: [
					'javascript-es2020', 'javascript-es2019', 'javascript-es2018',
					'javascript-es2017', 'javascript-es2016', 'javascript-es2015',
					'javascript' ],
				evaluatorUri: 'nowhere',
				evaluatorWs: 'somewhere',
				useReentrance: true
			},
			{
				programmingLanguages: [
					'python-3-9', 'python-3-8', 'python-3-7', 'python-3',
					'python' ],
				evaluatorUri: 'nowhere',
				evaluatorWs: 'somewhere',
				useReentrance: true
			}
		];
		const environment = {
			FUNCTION_EVALUATOR_URL: 'nowhere',
			FUNCTION_EVALUATOR_WS: 'somewhere'
		};
		const actualConfigs = getEvaluatorConfigsForEnvironment( environment, true );
		assert.deepEqual( actualConfigs, expectedConfigs );
	} );

} );
