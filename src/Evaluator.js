'use strict';

const fetch = require( '../lib/fetch.js' );
const { getLogger } = require( '../src/logger.js' );
const { convertWrappedZObjectToVersionedBinary } = require( '../function-schemata/javascript/src/serialize.js' );
const WebSocket = require( 'ws' );

const AVRO_SCHEMA_VERSION_ = '0.0.5';

/**
 * Function evaluator. Wraps API calls to the function-evaluator service, which
 * runs native code implementations.
 */
class Evaluator {
	constructor( evaluatorConfig ) {
		this.useReentrance_ = evaluatorConfig.useReentrance;
		this.evaluatorWs_ = evaluatorConfig.evaluatorWs;
		const logger = getLogger();
		if ( this.evaluatorWs_ === null && this.useReentrance_ ) {
			logger.warn(
				'trace/req',
				{
					msg: 'useReentrance was specified but no websocket location was supplied; setting useReentrance to false',
					evaluatorConfig: evaluatorConfig
				}
			);
			this.useReentrance_ = false;
		}
		this.evaluatorUri_ = evaluatorConfig.evaluatorUri;
		this.invariants_ = null;
		this.timeout_ = 10000; // wait 10 seconds
		this.programmingLanguages_ = Object.freeze( evaluatorConfig.programmingLanguages );
		Object.defineProperty( this, 'programmingLanguages', {
			get: function () {
				return this.programmingLanguages_;
			}
		} );
	}

	async evaluate( functionCall ) {
		if ( this.useReentrance_ ) {
			return await this.evaluateReentrant_( functionCall );
		} else {
			return await this.evaluate_( functionCall );
		}
	}

	async evaluate_( functionCall ) {
		const serialized = convertWrappedZObjectToVersionedBinary( {
			zobject: functionCall,
			reentrant: this.useReentrance_,
			remainingTime: this.invariants_.getRemainingTime(),
			requestId: this.invariants_.requestId
		}, /* version= */ AVRO_SCHEMA_VERSION_ );
		return await fetch(
			this.evaluatorUri_, {
				method: 'POST',
				body: serialized,
				headers: { 'Content-type': 'application/octet-stream' }
			}
		);
	}

	/*
     * Ignore the subsequent function for coverage purposes; we can't feasibly
     * unit-test this functionality.
     *
     * TODO (T322056): Make EvaluatorStub able to recognize and handle multiple
     * reentrant websocket-based function calls.
     */
	// istanbul ignore next
	async evaluateReentrant_( functionCall ) {
		const evaluatePromise = this.evaluate_( functionCall );
		const client = new WebSocket( this.evaluatorWs_ );
		const logger = getLogger();
		client.on( 'open', () => {
			logger.debug(
				'trace/req',
				{ msg: 'WS connection opened' }
			);
		} );
		client.on( 'message', async ( theMessage ) => {
			theMessage = theMessage.toString();
			logger.debug(
				'trace/req',
				{
					msg: 'message received',
					message: theMessage
				}
			);
			if ( theMessage.startsWith( 'call' ) ) {
				const { orchestrate } = require( './orchestrate' );
				theMessage = theMessage.replace( /^call\s*/, '' );
				const Z7 = JSON.parse( theMessage );
				if ( this.invariants_.getRemainingTime() > 0 ) {
					const normalResult = ( await orchestrate(
						Z7, this.invariants_, /* implementationSelector= */ null,
						/* returnNormal= */ true ) ).Z22K1;
					client.send( JSON.stringify( normalResult ) );
				}
				// TODO (T334485): Send a death threat to the evaluator if getRemainingTime
				// <= 0.
			}
		} );
		client.on( 'close', () => {
			logger.debug(
				'trace/req',
				{ msg: 'WS connection closed' }
			);
		} );
		const result = await evaluatePromise;
		// TODO: How to wait until connection opens?
		client.close();
		return result;
	}

	setInvariants( invariants ) {
		this.invariants_ = invariants;
	}
}

module.exports = { Evaluator };
